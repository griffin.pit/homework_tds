// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_TrashPiratesGameMode.h"
#include "TDS_TrashPiratesPlayerController.h"
#include "TDS_TrashPirates/Character/TDS_TrashPiratesCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_TrashPiratesGameMode::ATDS_TrashPiratesGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_TrashPiratesPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));

	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}