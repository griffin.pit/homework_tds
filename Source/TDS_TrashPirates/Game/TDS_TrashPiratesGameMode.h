// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_TrashPiratesGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_TrashPiratesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_TrashPiratesGameMode();
};



