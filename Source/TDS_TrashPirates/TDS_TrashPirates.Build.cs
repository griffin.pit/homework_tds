// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_TrashPirates : ModuleRules
{
	public TDS_TrashPirates(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
