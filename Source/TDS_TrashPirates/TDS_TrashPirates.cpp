// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_TrashPirates.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_TrashPirates, "TDS_TrashPirates" );

DEFINE_LOG_CATEGORY(LogTDS_TrashPirates)
 